package main

import (
	"fmt"
	"io"
	"log"
	"math"
	"math/rand"
	"time"

	"github.com/ebitengine/oto/v3"
)

const (
	sampleRate      = 44100
	channelNum      = 2
	bitDepthInBytes = 2
	bytesPerSample  = channelNum * bitDepthInBytes
	volumeFactor    = 0.5 // Volume adjustment factor
)

var decayFactor = math.Exp(-1.0 / 1000.0)

func whiteNoise(samples int) []float64 {
	noise := make([]float64, samples)
	for i := range noise {
		noise[i] = rand.Float64()*2 - 1 // Generate values in the range [-1, 1]
	}
	return noise
}

func applyFilter(samples []float64, prevFiltered float64, lowerBound, upperBound float64) ([]float64, float64) {
	filtered := make([]float64, len(samples))
	for i, value := range samples {
		if i == 0 {
			filtered[i] = decayFactor*prevFiltered + value
		} else {
			filtered[i] = decayFactor*filtered[i-1] + value
		}

		// Ensure the filtered value stays within the specified bounds
		if filtered[i] > upperBound {
			filtered[i] = upperBound
		} else if filtered[i] < lowerBound {
			filtered[i] = lowerBound
		}
	}
	return filtered, filtered[len(filtered)-1]
}

func leakyIntegrator(samples []float64, alpha float64, prevSample float64) ([]float64, float64) {
	integrated := make([]float64, len(samples))
	integrated[0] = alpha*prevSample + (1-alpha)*samples[0]
	for i := 1; i < len(samples); i++ {
		integrated[i] = alpha*integrated[i-1] + (1-alpha)*samples[i]
	}
	return integrated, integrated[len(integrated)-1]
}

func normalize(samples []float64) {
	max := 0.0
	for _, value := range samples {
		if abs(value) > max {
			max = abs(value)
		}
	}
	factor := 1.0 / max
	for i := range samples {
		samples[i] *= factor
	}
}

func abs(x float64) float64 {
	if x < 0 {
		return -x
	}
	return x
}

func convertToPCM(samples []float64) []byte {
	buffer := make([]byte, len(samples)*bytesPerSample)
	for i, sample := range samples {
		sampleValue := int16(sample * 32767.0 * volumeFactor)
		for c := 0; c < channelNum; c++ {
			buffer[i*bytesPerSample+c*2] = byte(sampleValue & 0xff)
			buffer[i*bytesPerSample+c*2+1] = byte(sampleValue >> 8)
		}
	}
	return buffer
}

func main() {
	alpha := 0.70 // Adjust the alpha value as needed

	// Initialize audio context
	op := &oto.NewContextOptions{
		SampleRate:   sampleRate,
		ChannelCount: channelNum,
		Format:       oto.FormatSignedInt16LE,
	}

	context, readyChan, err := oto.NewContext(op)
	if err != nil {
		log.Fatal(err)
	}
	<-readyChan

	prevSample := 0.0
	prevFiltered := 0.0

	reader, writer := io.Pipe()
	player := context.NewPlayer(reader)
	defer player.Close()

	go func() {
		defer writer.Close()
		for {
			totalSamples := sampleRate // Number of samples per second
			allSamples := make([]float64, 0, totalSamples)

			// Generate noise samples
			for len(allSamples) < totalSamples {
				whiteNoiseSamples := whiteNoise(sampleRate) // Generate white noise
				lowerBound := -20.0                         // Define your lower bound
				upperBound := 0.0                           // Define your upper bound

				filteredNoiseSamples, lastFiltered := applyFilter(whiteNoiseSamples, prevFiltered, lowerBound, upperBound)
				integratedSamples, lastSample := leakyIntegrator(filteredNoiseSamples, alpha, prevSample) // Apply leaky integrator with previous sample state
				normalize(integratedSamples)                                                              // Normalize the samples

				allSamples = append(allSamples, integratedSamples...)
				prevSample = lastSample     // Update the previous sample for the next iteration
				prevFiltered = lastFiltered // Update the previous filtered value for the next iteration
			}

			// Convert to PCM format
			buffer := convertToPCM(allSamples)

			// Write audio buffer to pipe
			_, err := writer.Write(buffer)
			if err != nil {
				log.Fatal(err)
			}
		}
	}()

	go player.Play()

	for {
		// Clear the previous line
		fmt.Print("\033[1K\r")

		// Get the amplitude of the current audio
		amplitude := prevFiltered

		// Print the amplitude
		fmt.Printf("Amplitude: %f", amplitude)

		// Wait for a short duration before updating again
		time.Sleep(time.Second)
	}
}
